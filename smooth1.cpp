#include <stdlib.h>
#include<stdio.h>

#include <omp.h>
#include <iostream>

using namespace std;
int counts;
int data[1000];

unsigned int leonardo[] = {
	1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973,
	3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049, 242785,
	392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155, 11405773,
	18454929, 29860703, 48315633, 78176337, 126491971, 204668309, 331160281,
	535828591, 866988873, 1402817465, 2269806339u, 3672623805u,
};
unsigned int times1 = 0;
unsigned int compare_time = 0;

void setvalue(int& t1, int& t2) {
	times1 += 1;
	t1 = t2;
	return;
}

int comp(int& t1, int& t2) {
	compare_time += 1;
	return t1 < t2;
}


void sortFix(
	int* array, int cheap, int index1, int* top1) {
	int prevh;
	int max1;
	int ch1;
	int ch2;
	int level2;

	while (index1 > 0) {
		prevh = cheap - leonardo[top1[index1]];
		if (comp(array[cheap], array[prevh])) {
			if (top1[index1] > 1) {
				ch1 = cheap - 1 - leonardo[top1[index1] - 2];
				ch2 = cheap - 1;
				if (comp(array[prevh], array[ch1])) break;
				if (comp(array[prevh], array[ch2])) break;
			}
			swaps(array[cheap], array[prevh]);
			cheap = prevh;
			index1 -= 1;
		}
		else break;
	}
	


	level2 = top1[index1];


	while (level2 > 1) {
		max1 = cheap;
		ch1 = cheap - 1 - leonardo[level2 - 2];
		ch2 = cheap - 1;

		if (comp(array[max1], array[ch1])) max1 = ch1;
		if (comp(array[max1], array[ch2])) max1 = ch2;
		if (max1 == ch1) {
			swap(array[cheap], array[ch1]);
			cheap = ch1;
			level2 -= 1;
		}
		else if (max1 == ch2) {
			swap(array[cheap], array[ch2]);
			cheap = ch2;
			level2 -= 2;
		}
		else break;
	}

	
	return;
}

void smoothSort(int* array, int size) {

	int top1[64] = { 1 };
	int toplevel = 0;
	int i;
	omp_set_num_threads(counts);

	for (i = 1; i < size; i++) {
#pragma omp parallel sections
{
	#pragma omp section
		if (toplevel > 0 && top1[toplevel - 1] - top1[toplevel] == 1) {
			toplevel -= 1;
			top1[toplevel] += 1;
		}
		else if (top1[toplevel] != 1) {
			toplevel += 1;
			top1[toplevel] = 1;
		}
		else {
			toplevel += 1;
			top1[toplevel] = 0;
		}
	#pragma omp section
		sortFix(array, i, toplevel, top1);
	}
	}

	for (i = size - 2; i > 0; i--) {
		if (top1[toplevel] <= 1) {
			toplevel -= 1;
		}
		else {
			top1[toplevel] -= 1;
			top1[toplevel + 1] = top1[toplevel] - 1;
			toplevel += 1;
#pragma omp parallel sections
{
	#pragma omp section
			sortFix(array, i - leonardo[top1[toplevel]], toplevel - 1, top1);
		#pragma omp section	
			sortFix(array, i, toplevel, top1);
}
		}
	}	
	return;
}
int main(int argc, char**argv)
{
	int tmp = 0, i, allSizes;
	counts = 1;
	cout<<"Enter size: ";
	scanf("%d", &allSizes);
	
	if (argc>1) {
		counts = atoi(argv[1]);
	}
	for (i = 0; i<allSizes; i++) {
		data[i]=rand()%allSizes+23;
	}

	for (i = 0; i<allSizes; i++)
	{
		cout<<" "<<data[i]<<" ";
	}
	cout<<endl;
	
	double start = omp_get_wtime();
	smoothSort(data, allSizes);
	double end = omp_get_wtime();
	for (i = 0; i<allSizes; i++)
	{
		cout<<" "<<data[i]<<" ";
	}
	cout<<endl<<"time: "<< end - start<<endl;
	
	return 0;
}
